import numpy as np

def next_square(a,b,c,d):
    a_prime = abs(a-b)
    b_prime = abs(b-c)
    c_prime = abs(c-d)
    d_prime = abs(d-a)
    return a_prime,b_prime,c_prime,d_prime

def f(a,b,c,d):
    i = 1
    while a > 0 or b > 0 or c > 0 or d > 0:
        print([a,b,c,d])
        a,b,c,d = next_square(a,b,c,d)
        i += 1
    return i

def backwards_construction(square):
    a = square[3] - square[1]
    b = square[0] - square[3]
    c = square[3]
    d = a/2 + b
    previous_square = [d+c,d-b,-d+c+b,d]
    for i in range(4):
        if square[i] > 10e6:
            print("square reached top limit")
            print(f"{square}")
            return 0
        if square[i] < 0:
            print("square reached lower limit")
            print(f"{square}")
            return 0
        if previous_square[i] > 10e6:
            print("square reached top limit")
            print(f"{square}")
            return 0
    if d!=int(d):
        print("previous_square is not constructable")
        print(f"{square}")
        return 0

    return previous_square

n=13
a = 2**n
b=0
c=2**n#(n+1)
d=0
square = [d+c,d-b,-d+c+b,d]
while square:
    ps = square
    square = backwards_construction(square)
    
print(f(*ps))
    
# first = np.array([730911, 4950551, 7244737, 8491626])
# f(*first)

# step = 1
# e1 = np.array([step,0,0,0])
# e2 = np.array([0,step,0,0])
# e3 = np.array([0,0,step,0])
# e4 = np.array([0,0,0,step])

# for i in range(1000000):
#     first = np.random.randint(0,10000000,4)
#     finish = 0
#     while not finish:
#         grad = np.zeros(4)

#         for e in [e1,e2,e3,e4]:
#             if f(*first)<f(*(first+e)):
#                 grad += e
#             elif f(*first)>f(*(first+e)):
#                 grad -= e

#         if np.linalg.norm(grad) == 0:
#             if f(*first)>=16:
#                 print(first)
#                 print(f(*first))
#             finish = 1
#         else:
#             first = first + grad


# for i in range(1000000000):
#     first_a,first_b,first_c,first_d = np.random.randint(0,10000000,4)
#     if f(first_a,first_b,first_c,first_d) >= 20:
#         print([first_a,first_b,first_c,first_d])
#         print(f(first_a,first_b,first_c,first_d))

        
print(f(ps[0], 0, ps[1],ps[1]+ps[2]))
