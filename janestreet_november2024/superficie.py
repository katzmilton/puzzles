import numpy as np
import matplotlib.pyplot as plt
def que_triangulo(x,y):
    if x > y:
        if y < 1 - x:
            return 0
        else:
            return 1
    else:
        if y > 1 - x:
            return 2
        else:
            return 3
        
def rot_m90(x, y, n):
    for rot in range(n):
        y_nuevo = 1 - x
        X_nuevo = y
        x = X_nuevo
        y = y_nuevo
    
    return [x, y]

#xa = np.linspace(0,1,1000)
#ya = np.linspace(0,1,1000)

xa = 0.2
ya = 0.25
xr = np.linspace(0,1,1000)

yr_min = []
yr_max = []
for x in xr:
    if x > xa:
        yr_max.append(min([np.sqrt(-2*xa + xa**2 + ya**2 + 2*x - x**2), 1]))
        yr_min.append(max([np.sqrt(xa**2+ya**2-x**2), 0]))
    else:
        yr_max.append(min([np.sqrt(xa**2+ya**2-x**2), 1]))
        yr_min.append(max([np.sqrt(-2*xa + xa**2 + ya**2 + 2*x - x**2), 0]))

plt.plot(xr, yr_max, label = "yr_max")
plt.plot(xr, yr_min, label = "yr_min")
#plt.plot(xr, np.sqrt(xa**2 + ya**2 - xr**2))
plt.legend()


plt.xlim([0,1])
plt.ylim([0,1])
plt.show()