from scipy.integrate import dblquad, tplquad
import numpy as np

def circ(xa,ya):
    return (np.pi/4)*(xa**2+ya**2) + (np.pi/4)*((xa-1)**2+ya**2)

def intercept1(xr,xa,ya):
    return -2*np.sqrt(2*(xr-xa)-xr**2+xa**2+ya**2)

def intercept2(xr,xa,ya):
    return -2*np.sqrt(xa**2+ya**2-xr**2)

integ_circ = dblquad(circ, 0, 0.5, lambda ya: ya, lambda ya: -ya+1, epsabs=1e-12)
print(integ_circ)

integ_intercept1 = tplquad(intercept1, 0, 0.5, lambda ya: ya, lambda ya: -ya+1, lambda ya, xa: 1 - np.sqrt((xa-1)**2 + ya**2), lambda ya,xa: xa, epsabs=1e-12)
print(integ_intercept1)
integ_intercept2 = tplquad(intercept2, 0, 0.5, lambda ya: ya, lambda ya: -ya+1, lambda ya,xa: xa, lambda ya,xa: np.sqrt(xa**2+ya**2), epsabs=1e-12)
print(integ_intercept2)

print(4*(integ_circ[0] + integ_intercept1[0] + integ_intercept2[0]))
# print(4*(integ_circ + integ_intercept1 + integ_intercept2[0]))
