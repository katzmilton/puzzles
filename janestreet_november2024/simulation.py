from numpy.random import rand

def que_triangulo(x,y):
    if x > y:
        if y < 1 - x:
            return 0
        else:
            return 1
    else:
        if y > 1 - x:
            return 2
        else:
            return 3
        
def rot_m90(x, y, n):
    for rot in range(n):
        y_nuevo = 1 - x
        X_nuevo = y
        x = X_nuevo
        y = y_nuevo
    return [x, y]


        
pos = 0
tot = 10000000
porc = 0
for i in range(tot):
    xr, yr, xa, ya = rand(4)
    triangulo = que_triangulo(xa, ya)
    xr, yr = rot_m90(xr, yr, triangulo)
    xa, ya = rot_m90(xa, ya, triangulo)
    p = (xr**2 + yr**2 - xa**2 - ya**2)/(2*(xr-xa))
    if p > 0 and p < 1:
        pos += 1
    if i % (tot/100) == 0:
        porc += 1
        print(porc, '%')

print(pos/tot)

